Para rodar o projeto siga os passos:

No Visual Studio:
1 - Faça o download/clone do projeto
2 - Abra o arquivo com extensão ".sln"
3 - Rode o projeto apertando a tecla F5

No VS Code:
1 - Faça o download/clone do projeto
2 - Abra o projeto no VS Code
3 - No terminal utilize o comando 'dotnet build' para fazer a build do projeto
4 - Após completar a build, utilize o comando 'dotnet run' para rodar o projeto
5 - Com a tecla CTRL apertada, clique no link do localhost para executar a aplicação em seu navegador


---------------

O projeto utiliza a versão 6 do .Net, caso sua versão esteja desatualizada, será necessário atualizá-la.