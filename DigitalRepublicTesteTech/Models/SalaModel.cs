﻿namespace DigitalRepublicTesteTech.Models
{
    public class SalaModel
    {
        public SalaModel()
        {

        }
        public SalaModel(ParedeModel paredeA, ParedeModel paredeB, ParedeModel paredeC, ParedeModel paredeD)
        {
            this.ParedeA = paredeA;
            this.ParedeB = paredeB;
            this.ParedeC = paredeC;
            this.ParedeD = paredeD;
            
        }

        public ParedeModel ParedeA { get; set; }
        public ParedeModel ParedeB { get; set; }
        public ParedeModel ParedeC { get; set; }
        public ParedeModel ParedeD { get; set; }

        public double AreaTotalJanelasEPortas { get; set; }
        public double AreaTotalQuePodeSerPintada { get; set; }

        public int TotalLatas18 { get; set; }
        public int TotalLatas3L600 { get; set; }
        public int TotalLatas2L5 { get; set; }
        public int TotalLatas0L5 { get; set; }

        public bool CalculoRealizado { get; set; } = false;


        public void CalcularLatasTotal()
        {
            CalcularLatas();
            this.CalculoRealizado = true;
        }

        public void CalcularAreaTotalJanelasEPortas()
        {
            this.AreaTotalJanelasEPortas = ParedeA.CalcularAreaJanelasEPortas() +
                                           ParedeB.CalcularAreaJanelasEPortas() +
                                           ParedeC.CalcularAreaJanelasEPortas() +
                                           ParedeD.CalcularAreaJanelasEPortas();
        }

        public void CalcularAreaTotalQuePodeSerPintada()
        {
            this.AreaTotalQuePodeSerPintada = ParedeA.CalcularAreaParede() +
                                              ParedeB.CalcularAreaParede() +
                                              ParedeC.CalcularAreaParede() +
                                              ParedeD.CalcularAreaParede();
        }

        public void CalcularLatas()
        {
            var qtd = AreaTotalQuePodeSerPintada;

            while (qtd > 0)
            {
                while (qtd >= 90)
                {
                    this.TotalLatas18++;
                    qtd -= 90;
                }
                while (qtd >= 18)
                {
                    this.TotalLatas3L600++;
                    qtd -= 18;
                }
                while (qtd >= 12.5)
                {
                    this.TotalLatas2L5++;
                    qtd -= 12.5;
                }
                while (qtd >= 2.5 || qtd <= 2.5 && qtd>=0)
                {
                    this.TotalLatas0L5++;
                    qtd -= 2.5;
                }
            }

        }

    }
}
