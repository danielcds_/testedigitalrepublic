﻿using DigitalRepublicTesteTech.Helpers;
using System.ComponentModel.DataAnnotations;

namespace DigitalRepublicTesteTech.Models
{
    public class ParedeModel : IParede
    {
        public ParedeModel()
        {
        }
        public ParedeModel(double comprimento, double altura, int qntPortas, int qntJanelas)
        {
            this.Comprimento = comprimento;
            this.Altura = altura;
            this.QntPortas = qntPortas;
            this.QntJanelas = qntJanelas;
        }

        [Required(ErrorMessage = "O campo Comprimento é obrigatório")]
        [RegularExpression(@"^([0-9]+(\,?[0-9]?[0-9]?)?)", ErrorMessage = "Campo precisa seguir o formato decimal, utilizando vírgula como separador")]
        public double? Comprimento { get; set; }

        [Required(ErrorMessage = "O campo Altura é obrigatório")]
        [RegularExpression(@"^([0-9]+(\,?[0-9]?[0-9]?)?)", ErrorMessage = "Campo precisa seguir o formato decimal, utilizando vírgula como separador.")]
        [ValidationAlturaPortas("QntPortas", ErrorMessage = "Quando houverem portas, a altura da parede deve ser no mínimo de 2,20m")]
        public double? Altura { get; set; }

        [Required(ErrorMessage = "O campo Quantidade de Portas é obrigatório")]
        public int? QntPortas { get; set; }

        [Required(ErrorMessage = "O campo Quantidade de Janelas é obrigatório")]
        public int? QntJanelas { get; set; }

        [ValidationAreaParede]
        public double AreaQuePodeSerPintada { get; set; }
        
        [ValidationAreaTotalJanelasEPortas("Comprimento", "Altura")]
        public double AreaTotalJanelaEPortas { get; set; }

        public double CalcularAreaJanelasEPortas()
        {
            if(QntJanelas != null && QntPortas != null)
            {
            this.AreaTotalJanelaEPortas = (2.4 * this.QntJanelas.Value) + (1.52 * this.QntPortas.Value);
            }
            return this.AreaTotalJanelaEPortas;
        }

        public double CalcularAreaParede()
        {
            if (Altura != null && Comprimento != null && AreaTotalJanelaEPortas != null)
                this.AreaQuePodeSerPintada = (Altura.Value * Comprimento.Value) - AreaTotalJanelaEPortas;
            return this.AreaQuePodeSerPintada;
        }

        

    }
}
