﻿namespace DigitalRepublicTesteTech.Models
{
    public interface IParede
    {
        public double? Comprimento { get; set; }
        public double? Altura { get; set; }
        public int? QntPortas { get; set; }
        public int? QntJanelas { get; set; }

    }
}
