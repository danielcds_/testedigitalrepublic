﻿using System.ComponentModel.DataAnnotations;

namespace DigitalRepublicTesteTech.Helpers
{
    public class ValidationAreaParede : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var areaTotal = (double)value;

                if (areaTotal < 1 || areaTotal > 50)
                {
                    return new ValidationResult("A parede deve ter uma área mínima de 1m² e máxima de 50m². O cálculo realizado é Comprimento x Altura e retira-se a área total de portas e janelas.");
                }
            }
            

            return ValidationResult.Success;

        }
    }
}
