﻿using System.ComponentModel.DataAnnotations;

namespace DigitalRepublicTesteTech.Helpers
{
    public class ValidationAreaTotalJanelasEPortas : ValidationAttribute
    {
        private readonly string _comprimento;
        private readonly string _altura;

        public ValidationAreaTotalJanelasEPortas(string comprimento, string altura)
        {
            _comprimento = comprimento;
            _altura = altura;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value != null)
            {
                var areaPortasEJanelas = (double)value;

            var propertyComprimento = validationContext.ObjectType.GetProperty(_comprimento);
            var valorObtidoComprimento = (double?)propertyComprimento.GetValue(validationContext.ObjectInstance);

            var propertyAltura = validationContext.ObjectType.GetProperty(_altura);
            var valorObtidoAltura = (double?)propertyAltura.GetValue(validationContext.ObjectInstance);

            var areaLimite = (valorObtidoComprimento * valorObtidoAltura) / 2;

                if (value != null && areaPortasEJanelas > areaLimite)
                {
                    return new ValidationResult("O total de área das portas e janelas deve ser no máximo 50% da área de parede");
                }
            }
            

            return ValidationResult.Success;

        }
    }
}
