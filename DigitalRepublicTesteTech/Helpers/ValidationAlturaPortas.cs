﻿using DigitalRepublicTesteTech.Models;
using System.ComponentModel.DataAnnotations;

namespace DigitalRepublicTesteTech.Helpers
{
    public class ValidationAlturaPortas : ValidationAttribute
    {
        private readonly string _quantidadePortas;

        public ValidationAlturaPortas(string quantidadePortas)
        {
            _quantidadePortas = quantidadePortas;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value != null)
            {
                var Altura = (double)value;

                var property = validationContext.ObjectType.GetProperty(_quantidadePortas);

                var valorObtidoQntPortas = (int)property.GetValue(validationContext.ObjectInstance);


                if (valorObtidoQntPortas > 0 && Altura < 2.20)
                {
                    return new ValidationResult("Quando houverem portas, a altura da parede deve ser no mínimo de 2,20m");
                }
            }
            
            
            return ValidationResult.Success;

        }
    }
}
