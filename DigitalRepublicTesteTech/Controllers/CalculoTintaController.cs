﻿using DigitalRepublicTesteTech.Models;
using Microsoft.AspNetCore.Mvc;
using System.Globalization;

namespace DigitalRepublicTesteTech.Controllers
{
    public class CalculoTintaController : Controller
    {
        
        
        [HttpGet]
        public IActionResult CalcularTinta()
        {
            return View();
        }


        [HttpPost]
        public IActionResult CalcularTinta(SalaModel sala)
        {

            sala.CalcularAreaTotalJanelasEPortas();
            sala.CalcularAreaTotalQuePodeSerPintada();

            ModelState.ClearValidationState(nameof(sala));
            ModelState.Clear();
            TryValidateModel(sala);
            
            if (ModelState.IsValid)
            {

                sala.CalcularLatasTotal();

                return View(sala);
            }

            return View(sala);
        }

    }
}
